import { Component, EventEmitter, OnInit, Output, Type } from '@angular/core';
import { initializeApp } from 'firebase/app';
import { Router } from '@angular/router';
import { getAuth, onAuthStateChanged, signOut } from 'firebase/auth';
import { MenuItem } from 'primeng/api';
import { AngularFireModule } from '@angular/fire/compat';

//@ts-ignore
import { getDatabase, onValue, ref, update } from 'firebase/database';

//@ts-ignore
import { getStorage, uploadBytes, type ref } from 'firebase/storage';

@Component({
  selector: 'app-superadmin',
  templateUrl: './superadmin.component.html',
  styleUrls: ['./superadmin.component.css'],
})
export class SuperadminComponent implements OnInit {
  //////Time////////
  current = new Date();
  timestamp = this.current.getTime();
  ///////////////
  //////////////////Edit Master/////////////
  Promotion = '';

  PriceRoomRegular = '';
  PriceRoomSuite = '';
  RoomSuite = '';
  RoomRegular = '';
  FacilityInRoom = '';
  FacilityIndoor = '';
  electricitybill = '';
  WaterPrice = '';
  TimeEdit = '';
  TypeRoomprice: any = [];
  /////////////////DisplayShow//////////////////
  displayshowDetailElecNWater = false;
  displayshowfacilityIndoor = false;
  displayshowPromotion = false;
  displayshowRoomsuite = false;
  displayshowRegular = false;
  displayshowPriceRoom = false;
  displayshowPicture = false;
  uploadedFiles: any[] = [];
  position!: string;
  //////////////////////////////////////////////
  items: MenuItem[] = [];

  showEdit = false;
  showUsers = false;
  ////////////// admin@dekhor.com ///////////////////
  firebaseConfig = initializeApp({
    apiKey: 'AIzaSyC0GQGYw6w_YvRQCpVrYJ0zU8dnlA9V9jI',
    authDomain: 'dekhordemo-23dde.firebaseapp.com',
    databaseURL:
      'https://dekhordemo-23dde-default-rtdb.asia-southeast1.firebasedatabase.app',
    projectId: 'dekhordemo-23dde',
    storageBucket: 'dekhordemo-23dde.appspot.com',
    messagingSenderId: '535683564882',
    appId: '1:535683564882:web:e419589efe9241723fe68b',
    measurementId: 'G-GCE1HL0W9B',
  });

  Pictureurl?: string;

  public database = getDatabase(this.firebaseConfig);
  Name: string = '';
  @Output() islogout = new EventEmitter<void>();

  constructor(private router: Router) {}

  ngOnInit(): void {
    const PicRef = ref(this.database, 'View/Pic/Picture');
    onValue(PicRef, (snapshot) => {
      const PicRefdata = snapshot.val();
      this.Pictureurl = PicRefdata.url;
    });
    const ElecRefs = ref(this.database, 'View/electricitybill');
    onValue(ElecRefs, (snapshot) => {
      const Billdata = snapshot.val();
      this.electricitybill = Billdata.Elec;
      this.WaterPrice = Billdata.Water;
      this.TimeEdit = Billdata.Last_Edit;
    });
    const FacilityInDoorRef = ref(this.database, 'View/' + 'FacilityIndoor');
    onValue(FacilityInDoorRef, (snapshot) => {
      const FacilityInDoorRefs = snapshot.val();

      this.FacilityIndoor = FacilityInDoorRefs.FacilityIndoor;
    });
    const PromotionRef = ref(this.database, 'View/' + 'Promotion');
    onValue(PromotionRef, (snapshot) => {
      const Promotions = snapshot.val();

      this.Promotion = Promotions.Promotion;
    });
    const RoomsuiteRef = ref(this.database, 'View/' + 'Roomsuite');
    onValue(RoomsuiteRef, (snapshot) => {
      const Roomsuites = snapshot.val();

      this.RoomSuite = Roomsuites.Roomsuite;
    });
    const RoomRegularRef = ref(this.database, 'View/' + 'RoomRegular');
    onValue(RoomRegularRef, (snapshot) => {
      const RoomRegulars = snapshot.val();

      this.RoomRegular = RoomRegulars.RoomRegular;
    });

    const TypeRoomprice = ref(this.database, 'TypeRooms');
    onValue(TypeRoomprice, (snapshot) => {
      const TypeRoompriceVal = snapshot.val();
      this.PriceRoomRegular = TypeRoompriceVal.Normal;
      this.PriceRoomSuite = TypeRoompriceVal.Suite;
    });

    const auth = getAuth();

    onAuthStateChanged(auth, (user) => {
      if (user == null) {
        // User is signed in, see docs for a list of available properties
        // https://firebase.google.com/docs/reference/js/firebase.User

        // alert('ท่านยังไม่ได้ล็อกอิน');
        location.replace('');

        // ...
      }
      if (user) {
        const uid = user.uid;
      }
    });

    this.items = [
      {
        label: 'บัญชีผู้ใข้งาน',
        icon: 'pi pi-fw pi-user',
        items: [
          {
            label: 'Info',
            icon: 'pi pi-fw pi-trash',
            command: () => {
              this.showdisplayUsers();
            },
          },
          {
            label: 'New',
            icon: 'pi pi-fw pi-plus',
            command: () => {
              this.showdisplayNewUser();
            },
          },
        ],
      },
      {
        label: 'จัดการแก้ไข',
        icon: 'pi pi-fw pi-pencil',
        items: [
          {
            label: 'ห้อง',
            icon: 'pi pi-fw pi-calendar-plus',
            items: [
              {
                label: 'ห้องสวีท',
                icon: 'pi pi-fw pi-trash',
                command: () => {
                  this.showdisplayRoomsuite('top');
                },
              },
              {
                label: 'ห้องปกติ',
                icon: 'pi pi-fw pi-plus',
                command: () => {
                  this.showdisplayRoomRegular('top');
                },
              },
              {
                label: 'แก้ไขราคาห้อง',
                icon: 'pi pi-fw pi-plus',
                command: () => {
                  this.showdisplayPrice('top');
                },
              },
            ],
          },
          {
            label: 'โปรโมชั่น',
            icon: 'pi pi-fw pi-calendar-minus',
            command: () => {
              this.showdisplayPromotion('top');
            },
          },
          {
            label: 'สิ่งอำนวยในอาคาร',
            icon: 'pi pi-fw pi-trash',
            command: () => {
              this.showdisplayfacilityIndoor('top');
            },
          },
          {
            label: 'รูปแนะนำหอพัก',
            icon: 'pi pi-fw pi-trash',
            command: () => {
              this.showDetailPicture('top');
            },
          },
          {
            label: 'ค่าน้ำค่าไฟในหอ',
            icon: 'pi pi-fw pi-trash',
            command: () => {
              this.showDetailElecNWater('top');
            },
          },
        ],
      },
      {
        label: 'ออกจากระบบ',
        icon: 'pi pi-fw pi-power-off',
        command: () => {
          this.logout();
        },
      },
    ];
  }

  logout() {
    this.Name = '';
    const auth = getAuth();
    signOut(auth)
      .then(() => {
        this.router.navigateByUrl('/welcome');
        alert('ออกจากระบบเรียบร้อย.');
      })
      .catch((error) => {
        console.log(error);
      });
  }
  showEditinformation() {
    this.showUsers = false;
    this.showEdit = true;
  }

  showManageUsers() {
    this.showUsers = true;
    this.showEdit = false;
  }
  showdisplayUsers() {
    this.showUsers = true;
  }
  showdisplayNewUser() {
    // this.displayshowNewrooms = true;
  }
  showDetailElecNWater(position: string) {
    this.position = position;
    this.displayshowDetailElecNWater = true;
  }
  showdisplayfacilityIndoor(position: string) {
    this.position = position;
    this.displayshowfacilityIndoor = true;
  }
  showdisplayPromotion(position: string) {
    this.position = position;
    this.displayshowPromotion = true;
  }
  showdisplayRoomRegular(position: string) {
    this.position = position;
    this.displayshowRegular = true;
  }
  showdisplayRoomsuite(position: string) {
    this.position = position;
    this.displayshowRoomsuite = true;
  }
  showdisplayPrice(position: string) {
    this.position = position;
    this.displayshowPriceRoom = true;
  }
  showDetailPicture(position: string) {
    this.position = position;
    this.displayshowPicture = true;
  }

  OnsaveBill() {
    if (!this.electricitybill && !this.WaterPrice) {
      alert('ว่างเปล่าาาาาาาาา');
    }

    update(ref(this.database, 'View/' + 'electricitybill'), {
      Elec: this.electricitybill,
      Water: this.WaterPrice,

      Last_Edit: this.timestamp,
    })
      .then(() => {
        alert('electricitybill saved successfully!');
      })
      .catch((error) => {
        // The write failed...
        alert(error);
      });
  }
  writefacilityIndoor() {
    if (!this.FacilityIndoor) {
      alert('ว่างเปล่าาาาาาาาา');
    }

    update(ref(this.database, 'View/' + 'FacilityIndoor'), {
      FacilityIndoor: this.FacilityIndoor,

      Last_Edit: this.timestamp,
    })
      .then(() => {
        alert('FacilityIndoor saved successfully!');
      })
      .catch((error) => {
        // The write failed...
        alert(error);
      });
  }
  writePromotion() {
    if (!this.Promotion) {
      alert('ว่างเปล่าาาาาาาาา');
    }

    update(ref(this.database, 'View/' + 'Promotion'), {
      Promotion: this.Promotion,

      Last_Edit: this.timestamp,
    })
      .then(() => {
        alert('Promotion saved successfully!');
      })
      .catch((error) => {
        // The write failed...
        alert(error);
      });
  }
  writeRoomsuite() {
    if (!this.RoomSuite) {
      alert('ว่างเปล่าาาาาาาาา');
    }

    update(ref(this.database, 'View/' + 'Roomsuite'), {
      Roomsuite: this.RoomSuite,

      Last_Edit: this.timestamp,
    })
      .then(() => {
        alert('Roomsuite saved successfully!');
      })
      .catch((error) => {
        // The write failed...
        alert(error);
      });
  }

  writeRoomRegular() {
    if (!this.RoomRegular) {
      alert('ว่างเปล่าาาาาาาาา');
    }

    update(ref(this.database, 'View/' + 'RoomRegular'), {
      RoomRegular: this.RoomRegular,

      Last_Edit: this.timestamp,
    })
      .then(() => {
        alert('RoomRegular saved successfully!');
      })
      .catch((error) => {
        // The write failed...
        alert(error);
      });
  }
  writePriceRoom() {
    if (!this.PriceRoomSuite) {
      alert('ว่างเปล่าาาาาาาาา');
    }

    update(ref(this.database, 'TypeRooms/'), {
      Normal: this.PriceRoomRegular,
      Suite: this.PriceRoomSuite,
      Last_Edit: this.timestamp,
    })
      .then(() => {
        alert('RoomRegular saved successfully!');
      })
      .catch((error) => {
        // The write failed...
        alert(error);
      });
  }
  onBasicUploadAuto(event: any) {
    for (let file of event.files) {
      this.uploadedFiles.push(file);
    }
  }
}
