import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { initializeApp } from 'firebase/app';
import {
  getAuth,
  createUserWithEmailAndPassword,
  deleteUser,
  updateEmail,
  updatePassword,
  reauthenticateWithCredential,
  signInWithEmailAndPassword,
} from 'firebase/auth';
import { getDatabase, ref, onValue, set, update } from 'firebase/database';
import * as firebase from 'firebase/app';
interface Gender {
  gender: string;
}

@Component({
  selector: 'app-manage-users',
  templateUrl: './manage-users.component.html',
  styleUrls: ['./manage-users.component.css'],
})
export class ManageUsersComponent implements OnInit {
  firebaseConfig = initializeApp({
    apiKey: 'AIzaSyC0GQGYw6w_YvRQCpVrYJ0zU8dnlA9V9jI',
    authDomain: 'dekhordemo-23dde.firebaseapp.com',
    databaseURL:
      'https://dekhordemo-23dde-default-rtdb.asia-southeast1.firebasedatabase.app',
    projectId: 'dekhordemo-23dde',
    storageBucket: 'dekhordemo-23dde.appspot.com',
    messagingSenderId: '535683564882',
    appId: '1:535683564882:web:e419589efe9241723fe68b',
    measurementId: 'G-GCE1HL0W9B',
  });
  displayshowdetail = false;
  displayshowEdit = false;
  displayshowRegister = false;
  displayshowDelete = false;

  ///////////Register////////////////////

  IsRegister = false;
  Islogin = false;

  inputRegisterID: string = '';
  inputRegisterEmail: string = '';
  inputRegisterPassword: string = '';
  inputRegisterFname: string = '';
  inputRegisterLname: string = '';
  inputRegisterBday: any = '';
  ID: any = '';

  gender!: Gender[];
  selectedGender!: Gender;
  inputRegisterPhone: string = '';

  //////////////////////////////////////////////
  /////////////////Show User data ///////////////////
  UsershowID: string = '';
  UsershowEmail: string = '';
  UsershowPassword: string = '';
  UsershowFname: string = '';
  UsershowLname: string = '';
  UsershowBday: string = '';
  UsershowGender: string = '';
  UsershowPhone: string = '';
  Modellenght: any = [];

  ////////////////////////////////////////////
  ///////////////////Edit//////////////////////
  inputEmailReAuth: string = '';
  inputPasswordReAuth: string = '';

  inputEditEmail: string = '';
  inputEditPassword: string = '';
  inputEditpersonalID: string = '';
  inputEditID: string = '';
  inputEditFname: string = '';
  inputEditLname: string = '';
  inputEditBday: string = '';
  inputEditGender: string = '';
  inputEditPhone: string = '';
  inputEditNumroom: string = '';
  inputEditPictrue: string = '';
  inputEditRole: string = '';

  pathtoEditID: string = '';
  ////////////////////////////////////////////

  ////////////////////////////////////////////

  //////////////////DELETE***/////////////////
  pathtoDeleteID: any = '';
  ////////////////////////////////////////////
  public auth = getAuth();
  public database = getDatabase(this.firebaseConfig);

  selectedNumroom: any;

  UserList: any = [];
  constructor(private router: Router) {
    this.UserList.length = 0;

    this.gender = [{ gender: 'ผู้ชาย' }, { gender: 'ผู้หญิง' }];

    const UserRefs = ref(this.database, 'Users/');
    onValue(UserRefs, (snapshot) => {
      snapshot.forEach((childSnapshot) => {
        const Userdata = childSnapshot.exportVal();
        const UserdataVal = childSnapshot.val();

        if (Userdata.role == 'Admin') {
          this.UserList.push(UserdataVal);
        }
      });
    });

    console.log(this.Modellenght.length);
  } //constructor

  ngOnInit(): void {}
  //ngOnInit

  showRegister() {
    this.displayshowRegister = true;
  }

  RegisterEmailPassword() {
    this.ID = this.inputRegisterEmail.slice(0, -11);
    const auth = getAuth();
    createUserWithEmailAndPassword(
      auth,
      this.inputRegisterEmail,
      this.inputRegisterPassword
    )
      .then((userCredential) => {
        // Signed in
        const user = userCredential.user;
        if (
          this.inputRegisterEmail == null ||
          this.inputRegisterEmail == '' ||
          this.inputRegisterEmail == ' '
        ) {
          return;
        } else {
          set(ref(this.database, 'Users/' + this.ID), {
            numroom: 'none',
            personalID: '',
            pictureUserUrl: '',
            id: this.ID,
            email: this.inputRegisterEmail,
            password: this.inputRegisterPassword,
            firstname: this.inputRegisterFname,
            lastname: this.inputRegisterLname,
            birthday: this.inputRegisterBday,
            gender: this.selectedGender,
            phone: this.inputRegisterPhone,
            role: 'Admin',
          });

          console.log('เพิ่ม' + this.inputRegisterID + 'เรียบร้อย');
          window.location.reload();
        }
        // ...
      })
      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
        console.log(errorMessage);
      });
  }

  showDetail(userid: string) {
    const useridRef = ref(this.database, 'Users/' + userid);
    onValue(useridRef, (snapshot) => {
      const Usershow = snapshot.val();
      this.UsershowID = Usershow.id;
      this.UsershowEmail = Usershow.email;
      this.UsershowPassword = Usershow.password;
      this.UsershowFname = Usershow.firstname;
      this.UsershowLname = Usershow.lastname;
      this.UsershowBday = Usershow.birthday;
      this.UsershowGender = Usershow.gender.gender;
      this.UsershowPhone = Usershow.phone;
    });

    this.displayshowdetail = true;
  }

  showEdit(email: string, Pwd: string, userid: string) {
    this.inputEmailReAuth = email;
    this.inputPasswordReAuth = Pwd;
    this.pathtoEditID = userid;
    const useridRef = ref(this.database, 'Users/' + this.pathtoEditID);
    onValue(useridRef, (snapshot) => {
      const Usershow = snapshot.val();
      this.inputEditEmail = Usershow.email;
      this.inputEditPassword = Usershow.password;
      this.inputEditpersonalID = Usershow.personalID;
      this.inputEditID = Usershow.id;
      this.inputEditFname = Usershow.firstname;
      this.inputEditLname = Usershow.lastname;
      this.inputEditBday = Usershow.birthday;
      this.inputEditGender = Usershow.gender.gender;
      this.inputEditPhone = Usershow.phone;
      this.inputEditNumroom = Usershow.numroom;
      this.inputEditPictrue = Usershow.pictureUserUrl;
      this.inputEditRole = Usershow.role;
    });
    this.displayshowEdit = true;

    console.log(this.inputEmailReAuth + this.inputPasswordReAuth);

    this.displayshowEdit = true;
  }
  showDelete(email: string, Pwd: string, userid: string) {
    this.inputEmailReAuth = email;
    this.inputPasswordReAuth = Pwd;
    this.pathtoDeleteID = userid;

    console.log(this.inputEmailReAuth + this.inputPasswordReAuth);

    this.displayshowDelete = true;
  }
  EditUser() {
    this.UserList.length = 0;
    const auth = getAuth();
    signInWithEmailAndPassword(
      auth,
      this.inputEmailReAuth,
      this.inputPasswordReAuth
    )
      .then((userCredential) => {
        // Signed in
        const user = auth.currentUser;
        console.log(user);

        const newPassword = this.inputEditPassword;
        updateEmail(user!, this.inputEditEmail)
          .then(() => {
            console.log('Update Emailsuccessful.');
            updatePassword(user!, newPassword)
              .then(() => {
                // Update successful.
                console.log('Update PWDsuccessful.');
                update(ref(this.database, 'Users/' + this.pathtoEditID), {
                  email: this.inputEditEmail,
                  password: this.inputEditPassword,
                  personalID: this.inputEditpersonalID,
                  id: this.inputEditID,
                  firstname: this.inputEditFname,
                  lastname: this.inputEditLname,
                  birthday: this.inputEditBday,
                  gender: { gender: this.inputEditGender },
                  phone: this.inputEditPhone,
                  numroom: this.inputEditNumroom,
                  pictureUserUrl: this.inputEditPictrue,
                  role: this.inputEditRole,
                });
                console.log('Update Database successful.');
              })
              .catch((error) => {
                // An error ocurred
                console.log(error);
                // ...
              });
          })
          .catch((error) => {
            console.log(error);
          });
      })
      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
        console.log(errorMessage);
      });
    setTimeout(() => {
      window.location.reload();
    }, 3000);
  }
  DeleteUser() {
    const auth = getAuth();
    signInWithEmailAndPassword(
      auth,
      this.inputEmailReAuth,
      this.inputPasswordReAuth
    )
      .then((userCredential) => {
        // Signed in
        const user = auth.currentUser;
        deleteUser(user!)
          .then(() => {
            // User deleted.
            console.log('User deleted.');
            if (
              this.pathtoDeleteID == '' &&
              this.pathtoDeleteID == null &&
              this.pathtoDeleteID == ''
            ) {
              console.log(this.pathtoDeleteID);
              return;
            } else {
              set(ref(this.database, 'Users/' + this.pathtoDeleteID), {});

              this.displayshowDelete = false;
            }
          })
          .catch((error) => {
            console.log(error);

            // An error ocurred
            // ...
          });
      })
      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
        console.log(errorMessage);
      });
    setTimeout(() => {
      window.location.reload();
    }, 2200);
  }
} ///end
