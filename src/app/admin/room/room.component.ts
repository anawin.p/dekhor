import { Component, Input, OnInit } from '@angular/core';
import { initializeApp } from 'firebase/app';
import { ref, onValue, getDatabase, set, update } from 'firebase/database';
import {
  ConfirmationService,
  MessageService,
  PrimeNGConfig
} from "primeng/api";

interface Floor {
  name: string;
}
interface UserModel {
  firstname: string;
  lastname: string;
}

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.css'],
  providers: [ConfirmationService, MessageService]
})
export class RoomComponent implements OnInit {
  firebaseConfig = initializeApp({
    apiKey: 'AIzaSyC0GQGYw6w_YvRQCpVrYJ0zU8dnlA9V9jI',
    authDomain: 'dekhordemo-23dde.firebaseapp.com',
    databaseURL:
      'https://dekhordemo-23dde-default-rtdb.asia-southeast1.firebasedatabase.app',
    projectId: 'dekhordemo-23dde',
    storageBucket: 'dekhordemo-23dde.appspot.com',
    messagingSenderId: '535683564882',
    appId: '1:535683564882:web:e419589efe9241723fe68b',
    measurementId: 'G-GCE1HL0W9B',
  });
  ContractdataIsEmpty=false;
  Contractdata: any = '';
  rooms: any = '';
  Roomlist: any = [];
  displayshowcontract = false;
  displayshowNewrooms = false;
  displayshowroom = false;
  firstnamelist: any = [];
  lastnamelist: any = [];
  namelist: any = [];
  codenamelist: any = [];
  selectroom: any = '';
  UserViewlist: UserModel[] = [];
  inputNumroom: any = '';

  RoomshowList: any = [];
  RoomSuiteList: any = [];
isEmtpy?:boolean;
isFree?:boolean;

  public database = getDatabase(this.firebaseConfig);


  constructor(
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private primengConfig: PrimeNGConfig
  ) {}

  confirm(event: Event,numroom: string) {
      const Room = numroom
    this.confirmationService.confirm({
      target: event.target!,
      message: "คุณต้องการลบห้องนี้ใช่หรือไม่",
      icon: "pi pi-exclamation-triangle",
      accept: () => {
        set(ref(this.database, 'Room/' + Room), {});
        window.location.reload()
       
      },
      reject: () => {
        this.messageService.add({
          severity: "error",
          summary: "Rejected",
          detail: "You have rejected"
        });
      }
    });
}



  ngOnInit(): void {
    this.primengConfig.ripple = true;

    const RoomSuiteRefs = ref(this.database, 'Room/');
    onValue(RoomSuiteRefs, (snapshot) => {
      snapshot.forEach((childSnapshot) => {
        const Userdata = childSnapshot.exportVal();
        const UserdataVal = childSnapshot.val();
        const SizeData = childSnapshot.size;
        if (SizeData == 1) {
          
          this.isEmtpy=true
          
          
        }
        else{

          this.isFree=true
          

        }
      });
    });

console.log(this.Roomlist);

  
    const UserRefs = ref(this.database, 'Room');
    onValue(UserRefs, (snapshot) => {
      snapshot.forEach((childSnapshot) => {
        const Userdata = childSnapshot.key;

        this.Roomlist.push(Userdata);
      });
    });
  }
  showNewrooms() {
    this.displayshowNewrooms = true;
  }

  showRoom(numroom: string) {
    this.rooms = numroom;
    this.codenamelist.length = 0;
    this.UserViewlist.length = 0;
    this.namelist.length = 0;

    const RoomRefs = ref(this.database, 'Room/' + numroom);
    onValue(RoomRefs, (snapshot) => {
      snapshot.forEach((childSnapshot) => {
        const Userdata = childSnapshot.val();
        this.codenamelist.push(Userdata);
      });
    });
    console.log(this.codenamelist);

    const UserRefs = ref(this.database, 'Users/');
    onValue(UserRefs, (snapshot) => {
      let num = this.codenamelist;
      num.forEach((value: string) => {
        if (snapshot.hasChild(value)) {
          const UserViewRefs = ref(this.database, 'Users/' + value);
          onValue(UserViewRefs, (snapshot) => {
            const UserView = snapshot.val();
            this.UserViewlist.push(UserView);
            console.log(UserView);

            // this.UserViewlist.push(UserView.firstname);
            // this.UserViewlist.push(UserView.lastname);
          });
          console.log('มี' + value);
        }
      });
    });
    console.log(this.UserViewlist);

    this.displayshowroom = true;
  }

  
  Displaycontract(room:string) {
    this.rooms =room;
    this.ContractdataIsEmpty=false
    this.Contractdata = '';
    this.displayshowroom = false;
    this.displayshowcontract = true;

    const ConreactRefs = ref(this.database, 'Contract/');
    onValue(ConreactRefs, (snapshot) => {
      snapshot.forEach((childSnapshot) => {
        const Conreactdata = childSnapshot.exportVal();
        const ConreactdataVal = childSnapshot.val();

        if (Conreactdata.name == room) {
          this.Contractdata = ConreactdataVal.url;

        }

      }
      );    
      if(this.Contractdata===''){
        this.ContractdataIsEmpty=true
  
      }
    });

  }

}
