import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { initializeApp } from 'firebase/app';
import { getAuth, onAuthStateChanged, signOut } from 'firebase/auth';
import { getDatabase, onValue, ref, set, update } from 'firebase/database';
import {
  ConfirmationService,
  MessageService,
  PrimeNGConfig
} from "primeng/api";

import { MenuItem } from 'primeng/api';
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css'],
})
export class AdminComponent implements OnInit {
  ContractdataIsEmpty=false;
  rooms: any = '';
  displayshowroom=false;
  displayshowcontract?:boolean;
  Contractdata='';
  selectTypeRoom: string = '';
  stateOptions: any = [];
  value1: string = '';
  Roomlist:any=[];
  items: MenuItem[] = [];
  ///////Repair//////
  state: string = '';

  PathRepairRoom: any;

  Repair_numroom: any;
  Repair_titleRepair: any;
  Repair_phone: any;
  Repair_cost: any;
  Repair_detail: any;
  Repair_imageUrl: any;
  ///////////////

  showcostutilities = false;
  showrooms = false;
  showuser = false;
  showinvoice = false;
  shownewroom = false;
  showcontract = false;
  showtechnician=false;

  displayshowDetailRepair = false;

  Repairlist: any = [];
  Repairdatalist: any = [];
  repairStateforwand?:boolean;
  repairStatesuscess?:boolean;
  repairStatewaittechnicain?:boolean;
  repairStatewaitNiti?:boolean;

  ////////////// admin@dekhor.com ///////////////////
  firebaseConfig = initializeApp({
    apiKey: 'AIzaSyC0GQGYw6w_YvRQCpVrYJ0zU8dnlA9V9jI',
    authDomain: 'dekhordemo-23dde.firebaseapp.com',
    databaseURL:
      'https://dekhordemo-23dde-default-rtdb.asia-southeast1.firebasedatabase.app',
    projectId: 'dekhordemo-23dde',
    storageBucket: 'dekhordemo-23dde.appspot.com',
    messagingSenderId: '535683564882',
    appId: '1:535683564882:web:e419589efe9241723fe68b',
    measurementId: 'G-GCE1HL0W9B',
  });

  public database = getDatabase(this.firebaseConfig);
  Name: string = '';
  @Output() islogout = new EventEmitter<void>();
  inputNumroom: any;


  constructor(private router: Router,
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private primengConfig: PrimeNGConfig) {
    this.stateOptions = [
      {
        label: 'ห้องปกติ',
        value: 'Normal',
      },
      {
        label: 'ห้องสวีท',
        value: 'Suite',
      },
    ];
  }

  ngOnInit(): void {
    const statepage = sessionStorage.getItem('statuspage');
    const auth = getAuth();

    onAuthStateChanged(auth, (user) => {
      if (user == null) {
        // User is signed in, see docs for a list of available properties
        // https://firebase.google.com/docs/reference/js/firebase.User

        // alert('ท่านยังไม่ได้ล็อกอิน');
        location.replace('');

        // ...
      }
      if (user) {
        const uid = user.uid;
      }
    });

    
    this.items = [
      {
        label: 'ห้อง',
        icon: 'pi pi-fw pi-file',
        items: [
          {
            label: 'จัดการห้อง',
            icon: 'pi pi-fw pi-trash',

            items: [
              {
                label: 'ห้องทั้งหมด',
                icon: 'pi pi-fw pi-plus',
                command: () => {
                  this.showdisplayANY('allroom');
                },
              },
              {
              label: 'ห้องว่าง',
              icon: 'pi pi-fw pi-plus',
              command: () => {
                this.showdisplayANY('RoomEmpty');
              },
            },
            {
              label: 'ห้องที่ใช้งาน',
              icon: 'pi pi-fw pi-plus',
              command: () => {
                this.showdisplayANY('RoomUsed');
              },
            },
          ],
            
          },
          {
            label: 'สร้างห้องใหม่',
            icon: 'pi pi-fw pi-plus',
            command: () => {
              this.showdisplayANY('newroom');
            },
          },
          {
            label: 'บันทึกสัญญาเช่า',
            icon: 'pi pi-fw pi-plus',
            command: () => {
              this.showdisplayANY('Contract');
            },
          },
        ],
      },
      {
        label: 'บัญชีผู้ใช้งาน',
        icon: 'pi pi-fw pi-user',
        items: [
          {
            label: 'ตารางผู้ใช้งาน',
            icon: 'pi pi-fw pi-trash',
            command: () => {
              this.showdisplayANY('Users');
            },
          },
           { 
            label: 'ตารางช่าง',
            icon: 'pi pi-fw pi-plus',
            command: () => {
              this.showdisplayANY('Technician');
            },
          }, 
        ],
      },
      {
        label: 'แจ้งซ่อม',
        icon: 'pi pi-fw pi-pencil',
        items: [
          {
            label: 'รายการใหม่',
            icon: 'pi pi-fw pi-calendar-plus',
            command: () => {
              this.showdisplayANY('Fixyet');
            },
          },
          {
            label: 'งานที่มอบหมาย',
            icon: 'pi pi-fw pi-calendar-minus',
            command: () => {
              this.showdisplayANY('Fixyetadminforward');
            },
          },
          {
            label: 'ตรวจสอบงาน',
            icon: 'pi pi-fw pi-calendar-minus',
            command: () => {
              this.showdisplayANY('Fixyettechforward');
            },
          },
          {
            label: 'ประวัติการซ่อม',
            icon: 'pi pi-fw pi-calendar-minus',
            command: () => {
              this.showdisplayANY('Fixed');
            },
          },
        ],
      },
      {
        label: 'จัดการบิล',
        icon: 'pi pi-fw pi-calendar',
        items: [
          {
            label: 'สร้างบิล',
            icon: 'pi pi-fw pi-pencil',
            command: () => {
              this.showdisplayANY('invoice');
            },
          },
          {
            label: 'คำนวณค่าไฟ',
            icon: 'pi pi-fw pi-calendar-times',
            command: () => {
              this.showdisplayANY('Utilities');
            },
          },
        ],
      },
      {
        label: 'ออกจากระบบ',
        icon: 'pi pi-fw pi-power-off',
        command: () => {
          this.logout();
        },
      },
    ];
  }

  logout() {
    this.Name = '';
    const auth = getAuth();
    signOut(auth)
      .then(() => {
        alert('ออกจากระบบเรียบร้อย.');
        this.router.navigateByUrl('/welcome');
      })
      .catch((error) => {
        console.log(error);
      });
  }
  newrooms(RoomType: string) {
    const Numroom = this.inputNumroom;

    if (RoomType == '') {
      return;
    } else {
      const database = getDatabase(this.firebaseConfig);
      update(ref(database, 'Room/' + Numroom), {
        Type: RoomType,
      });

      window.location.href = '/admin';
    }
  }

  showDetailRepair(Numroom: string, timestamp: string) {
    this.displayshowDetailRepair = true;

    const RepairRefs = ref(this.database, 'Repair/');
    onValue(RepairRefs, (snapshot) => {
      snapshot.forEach((childSnapshot) => {
        const Repairdata = childSnapshot.exportVal();
        if (Repairdata.timestamp == timestamp) {
          this.Repair_numroom = Repairdata.numroom;
          this.Repair_titleRepair = Repairdata.titleRepair;
          this.Repair_phone = Repairdata.phone;
          this.Repair_cost = Repairdata.cost;
          this.Repair_detail = Repairdata.detail;
          this.Repair_imageUrl = Repairdata.imageUrl;
        }
      });
    });
  }
  showdisplayANY(state: string) {
    this.state = state;
    if (this.state == 'Fixyet') {
      this.Repairdatalist.length = 0;
      this.Roomlist.length=0;
      this.showrooms = false;
      this.showuser = false;
      this.showinvoice = false;
      this.showcostutilities = false;
      this.showcontract = false;
      this.showtechnician=false;
      this.Fixyet();
    }
    if (this.state == 'Fixed') {
      this.Repairdatalist.length = 0;
      this.Roomlist.length=0;
      this.showrooms = false;
      this.showuser = false;
      this.showinvoice = false;
      this.showcostutilities = false;
      this.showcontract = false;
      this.showtechnician=false;
      this.Fixed();
    }
    if (this.state == 'Fixyetadminforward') {
      this.Repairdatalist.length = 0;
      this.Roomlist.length=0;
      this.showrooms = false;
      this.showuser = false;
      this.showinvoice = false;
      this.showcostutilities = false;
      this.showcontract = false;
      this.showtechnician=false;
      this.Fixyetadminforward();
    }
    if (this.state == 'Fixyettechforward') {
      this.Repairdatalist.length = 0;
      this.Roomlist.length=0;
      this.showrooms = false;
      this.showuser = false;
      this.showinvoice = false;
      this.showcostutilities = false;
      this.showcontract = false;
      this.showtechnician=false;
      this.Fixedyettechforward();
    }
    if (this.state == 'RoomEmpty') {
      this.showrooms = false;
      this.Roomlist.length=0;
      this.Repairdatalist.length = 0;
      this.showuser = false;
      this.showinvoice = false;
      this.showcostutilities = false;
      this.showcontract = false;
      this.showtechnician=false;
      this.RoomEmpty();
      
    }
    if (this.state == 'RoomUsed') {
      this.showrooms = false;
      this.Roomlist.length=0;
      this.Repairdatalist.length = 0;
      this.showuser = false;
      this.showinvoice = false;
      this.showcostutilities = false;
      this.showcontract = false;
      this.showtechnician=false;
      this.RoomUsed();
    }
    if (this.state == 'newroom') {
      this.Repairdatalist.length = 0;
      this.Roomlist.length=0;
      this.showuser = false;
      this.showinvoice = false;
      this.showcostutilities = false;
      this.showcontract = false;
      this.showrooms = true;
      this.showtechnician=false;
      this.shownewroom = true;
    }
    if (this.state == 'Users') {
      this.Repairdatalist.length = 0;
      this.Roomlist.length=0;
      this.showrooms = false;
      this.showinvoice = false;
      this.showcostutilities = false;
      this.showcontract = false;
      this.showtechnician=false
      this.showuser = true;
      
    }
    if (this.state == 'Technician') {
      this.Repairdatalist.length = 0;
      this.Roomlist.length=0;
      this.showrooms = false;
      this.showuser = false;
      this.showinvoice = false;
      this.showcostutilities = false;
      this.showcontract = false;
      this.showtechnician=true
    }
    if (this.state == 'invoice') {
      this.Repairdatalist.length = 0;
      this.Roomlist.length=0;
      this.showrooms = false;
      this.showuser = false;
      this.showcontract = false;
      this.showcostutilities = false;
      this.showinvoice = true;
      this.showtechnician=false;
    }
    if (this.state == 'Utilities') {
      this.Repairdatalist.length = 0;
      this.Roomlist.length=0;
      this.showrooms = false;
      this.showuser = false;
      this.showinvoice = false;
      this.showcontract = false;
      this.showtechnician=false;
      this.showcostutilities = true;
    }
    if (this.state == 'Contract') {
      this.Repairdatalist.length = 0;
      this.Roomlist.length=0;
      this.showrooms = false;
      this.showuser = false;
      this.showinvoice = false;
      this.showcostutilities = false;
      this.showtechnician=false;
      this.showcontract = true;
    }
    if (this.state == 'allroom') {
      this.Repairdatalist.length = 0;
      this.Roomlist.length=0;
      this.showrooms = true;
      this.showuser = false;
      this.showinvoice = false;
      this.showcostutilities = false;
      this.showtechnician=false;
      this.showcontract = false;
    }
  }
  Fixyet() {
    const RepairRefs = ref(this.database, 'Repair/');
    onValue(RepairRefs, (snapshot) => {
      snapshot.forEach((childSnapshot) => {
        const Repairdata = childSnapshot.exportVal();
        const RepairdataVal = childSnapshot.val();

        if (Repairdata.status == 0) {
          this.Repairdatalist.push(RepairdataVal);
          this.repairStatesuscess=false;
          this.repairStatewaittechnicain=false;
          this.repairStatewaitNiti=false

          this.repairStateforwand=true;
         
        }
      });
    });
  }
  Fixed() {
    const RepairRefs = ref(this.database, 'Repair/');
    onValue(RepairRefs, (snapshot) => {
      snapshot.forEach((childSnapshot) => {
        const Repairdata = childSnapshot.exportVal();
        const RepairdataVal = childSnapshot.val();

        if (Repairdata.status == 1) {
          this.Repairdatalist.push(RepairdataVal);
          this.repairStateforwand=false;
          this.repairStatewaittechnicain=false;
          this.repairStatewaitNiti=false;

          this.repairStatesuscess=true;
          
          
        }
      });
    });
  }
  Fixyetadminforward() {
    const RepairRefs = ref(this.database, 'Repair/');
    onValue(RepairRefs, (snapshot) => {
      snapshot.forEach((childSnapshot) => {
        const Repairdata = childSnapshot.exportVal();
        const RepairdataVal = childSnapshot.val();

        if (Repairdata.status == 2) {
          this.Repairdatalist.push(RepairdataVal);
          this.repairStateforwand=false;
          this.repairStatesuscess=false;
          this.repairStatewaitNiti=false;

          this.repairStatewaittechnicain=true;
        }
      });
    });
  }
  Fixedyettechforward() {
    const RepairRefs = ref(this.database, 'Repair/');
    onValue(RepairRefs, (snapshot) => {
      snapshot.forEach((childSnapshot) => {
        const Repairdata = childSnapshot.exportVal();
        const RepairdataVal = childSnapshot.val();
console.log(RepairdataVal);

        if (Repairdata.status == 3) {
          this.Repairdatalist.push(RepairdataVal);
          this.repairStateforwand=false;
          this.repairStatesuscess=false;
          this.repairStatewaittechnicain=false;

          this.repairStatewaitNiti=true
        }
      });
    });
  }

  RoomUsed(){
    const RoomSuiteRefs = ref(this.database, 'Room/');
    onValue(RoomSuiteRefs, (snapshot) => {
      snapshot.forEach((childSnapshot) => {
        const Userdata = childSnapshot.key;
        const UserdataVal = childSnapshot.val();
        const SizeData = childSnapshot.size;
        if (SizeData > 1) {
          
          this.Roomlist.push(Userdata)
          
          
        }

      });
    });

  }
  RoomEmpty(){
    const RoomSuiteRefs = ref(this.database, 'Room/');
    onValue(RoomSuiteRefs, (snapshot) => {
      snapshot.forEach((childSnapshot) => {
        const Userdata = childSnapshot.key;
        const UserdataVal = childSnapshot.val();
        const SizeData = childSnapshot.size;
        if (SizeData == 1) {
          
          this.Roomlist.push(Userdata)
          
        }
      });
    });

  }
  Displaycontract(room:string) {
    this.rooms =room;
    this.ContractdataIsEmpty=false
    this.Contractdata = '';
    this.displayshowroom = false;
    this.displayshowcontract = true;

    const ConreactRefs = ref(this.database, 'Contract/');
    onValue(ConreactRefs, (snapshot) => {
      snapshot.forEach((childSnapshot) => {
        const Conreactdata = childSnapshot.exportVal();
        const ConreactdataVal = childSnapshot.val();

        if (Conreactdata.name == room) {
          this.Contractdata = ConreactdataVal.url;

        }

      }
      );    
      if(this.Contractdata===''){
        this.ContractdataIsEmpty=true
  
      }
    });

  }
  confirm(event: Event,numroom: string) {
    const Room = numroom
  this.confirmationService.confirm({
    target: event.target!,
    message: "คุณต้องการลบห้องนี้ใช่หรือไม่",
    icon: "pi pi-exclamation-triangle",
    accept: () => {
      set(ref(this.database, 'Room/' + Room), {});
      window.location.reload()
     
    },
    reject: () => {
      this.messageService.add({
        severity: "error",
        summary: "Rejected",
        detail: "You have rejected"
      });
    }
  });
}


  Onsave(){
    
  }
}
