import { getLocaleDateTimeFormat } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { initializeApp } from 'firebase/app';
import { ref, onValue, getDatabase, set } from 'firebase/database';
import dateFormat from 'dateformat';

@Component({
  selector: 'app-costutilities',
  templateUrl: './costutilities.component.html',
  styleUrls: ['./costutilities.component.css'],
})
export class CostutilitiesComponent implements OnInit {
  firebaseConfig = initializeApp({
    apiKey: 'AIzaSyC0GQGYw6w_YvRQCpVrYJ0zU8dnlA9V9jI',
    authDomain: 'dekhordemo-23dde.firebaseapp.com',
    databaseURL:
      'https://dekhordemo-23dde-default-rtdb.asia-southeast1.firebasedatabase.app',
    projectId: 'dekhordemo-23dde',
    storageBucket: 'dekhordemo-23dde.appspot.com',
    messagingSenderId: '535683564882',
    appId: '1:535683564882:web:e419589efe9241723fe68b',
    measurementId: 'G-GCE1HL0W9B',
  });
  public database = getDatabase(this.firebaseConfig);

  IsCount = false;

  RoomNormal: any;
  RoomSuite: any;
  displayshowroom = false;
  RoomType: any = '';
  roomprice: number = 0;
  rooms: any = '';
  Roomlist: any = [];
  ElectricBefore: any = '';
  ElectricAfter: any = '';
  WaterBefore: any = '';
  WaterAfter: any = '';
  Sum: any = '';
  electricitybill: any = '';
  WaterPrice: any = '';
  SumElectric?: number;
  SumWater?: number;
  Realroomprice?: number;

  date9!: Date;
  now = new Date();
  modelmonth?: string;
  Realmonth?: number;
  D = new Date();
  Month = this.D.getMonth();
  Year = this.D.getFullYear();

  constructor() {
    this.modelmonth = dateFormat(this.now, 'm');
    this.Realmonth = parseFloat(this.modelmonth) - 1;

    const CostRefs = ref(this.database, 'Room');

    onValue(CostRefs, (snapshot) => {
      snapshot.forEach((childSnapshot) => {
        // this.MessageList = childSnapshot.key;
        const Userdata = childSnapshot.key;
        this.Roomlist.push(Userdata);
      });
    });
  }

  ngOnInit(): void {
    const ElecRefs = ref(this.database, 'View/electricitybill');
    onValue(ElecRefs, (snapshot) => {
      const Billdata = snapshot.val();
      this.electricitybill = Billdata.Elec;
      this.WaterPrice = Billdata.Water;
    });

    const TypeRoomRef = ref(this.database, 'TypeRooms/');
    onValue(TypeRoomRef, (snapshot) => {
      const TypeRoomdata = snapshot.val();
      this.RoomNormal = TypeRoomdata.Normal;
      this.RoomSuite = TypeRoomdata.Suite;
    });
  }

  showRoom(room: any) {
    this.rooms = room;
    this.displayshowroom = true;
    const RoomTypeRefs = ref(this.database, 'Room/' + this.rooms);
    onValue(RoomTypeRefs, (snapshot) => {
      const RoomTypedata = snapshot.val();
      this.RoomType = RoomTypedata.Type;
    });
    console.log(this.RoomType);
    if (this.RoomType === 'Normal') {
      this.roomprice = this.RoomNormal;
    } else if (this.RoomType === 'Suite') {
      this.roomprice = this.RoomSuite;
    }
    console.log(this.roomprice);
  }
  IsCalculate() {
    if (
      this.ElectricBefore === '' &&
      this.ElectricAfter === '' &&
      this.WaterAfter === '' &&
      this.WaterBefore === ''
    ) {
      return;
    } else {
      const roomprice = +this.roomprice; // + แปล เป็น Number //
      this.Realroomprice = roomprice;
      this.SumElectric =
        (this.ElectricAfter - this.ElectricBefore) * this.electricitybill;
      this.SumWater = (this.WaterAfter - this.WaterBefore) * this.WaterPrice;
      this.Sum = this.SumElectric + this.SumWater + roomprice;
    }
    if (this.Sum == null) {
      console.log('IsEmty');
    } else {
      this.IsCount = true;
    }
  }

  Onsave() {
    set(
      ref(
        this.database,
        'Bills/' + this.Year + '/' + this.Month + '/' + this.rooms
      ),
      {
        discount: '0',
        electricity: this.SumElectric,
        imageUrl: '',
        roomprice: this.Realroomprice,
        status: '0',
        sum: this.Sum,
        water: this.SumWater,
        elecbefore: this.ElectricBefore,
        elecafter: this.ElectricAfter,
        waterafter: this.WaterAfter,
        waterbefore: this.WaterBefore,
        fee: '',
      }
    );
    this.displayshowroom = false;
  }
}
