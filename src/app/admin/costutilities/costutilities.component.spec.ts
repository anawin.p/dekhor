import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CostutilitiesComponent } from './costutilities.component';

describe('CostutilitiesComponent', () => {
  let component: CostutilitiesComponent;
  let fixture: ComponentFixture<CostutilitiesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CostutilitiesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CostutilitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
