import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { initializeApp } from 'firebase/app';
import {
  getAuth,
  createUserWithEmailAndPassword,
  deleteUser,
  updateEmail,
  updatePassword,
} from 'firebase/auth';
import firebase from 'firebase/compat/app';

import {
  getDatabase,
  ref,
  onValue,
  set,
  update,
  query,
  orderByChild,
  equalTo,
  remove,
} from 'firebase/database';

interface Gender {
  gender: string;
}

@Component({
  selector: 'app-manage-technicain',
  templateUrl: './manage-technicain.component.html',
  styleUrls: ['./manage-technicain.component.css']
})
export class ManageTechnicainComponent implements OnInit {
  firebaseConfig = initializeApp({
    apiKey: 'AIzaSyC0GQGYw6w_YvRQCpVrYJ0zU8dnlA9V9jI',
    authDomain: 'dekhordemo-23dde.firebaseapp.com',
    databaseURL:
      'https://dekhordemo-23dde-default-rtdb.asia-southeast1.firebasedatabase.app',
    projectId: 'dekhordemo-23dde',
    storageBucket: 'dekhordemo-23dde.appspot.com',
    messagingSenderId: '535683564882',
    appId: '1:535683564882:web:e419589efe9241723fe68b',
    measurementId: 'G-GCE1HL0W9B',
  });
  displayshowdetail = false;
  displayshowEdit = false;
  displayshowRegister = false;
  displayshowDelete = false;
  displayshowRegistertechnician=false;
  ///////////Register////////////////////

  IsRegister = false;
  Islogin = false;

  inputRegisterID: string = '';
  inputRegisterEmail: string = '';
  inputRegisterPassword: string = '';
  inputRegisterFname: string = '';
  inputRegisterLname: string = '';
  inputRegisterpersonalID: any = '';
  gender!: Gender[];
  selectedGender!: Gender;

  inputRegisterPhone: string = '';

  //////////////////////////////////////////////
  /////////////////Show User data ///////////////////
  UsershowID: string = '';
  UsershowEmail: string = '';
  UsershowPassword: string = '';
  UsershowFname: string = '';
  UsershowLname: string = '';
  UsershowBday: string = '';
  UsershowGender: string = '';
  UsershowPhone: string = '';
  UsershowNumroom: string = '';
  UsershowPersonalID: string = '';
  UsershowPictrue: string = '';
  UsershowRole: string = '';
  Modellenght: any = [];

  ////////////////////////////////////////////
  ///////////////////Edit//////////////////////
  inputEditEmail: string = '';
  inputEditPassword: string = '';
  inputEditpersonalID: string = '';
  inputEditID: string = '';
  inputEditFname: string = '';
  inputEditLname: string = '';
  inputEditBday: string = '';
  inputEditGender: string = '';
  inputEditPhone: string = '';
  inputEditNumroom: string = '';
  inputEditPictrue: string = '';
  inputEditRole: string = '';

  pathtoEditID: string = '';
  //////////////////Register//////////////////
  pathtoRegisterID: any = '';
  pathtoRegisterUserInroom: any = '';
  RegisterP1: any;
  RegisterP2: any;
  RegisterP3: any;
  RegisterTypeRoom: any;
  //////////////////DELETE***/////////////////
  pathtoDeleteID: any = '';
  pathtoDeleteUserInroom: any = '';
  Roomlist: any = [];
  DeleteTypeRoom: any;
  DeleteP1: any;
  DeleteP2: any;
  DeleteP3: any;
  nametechnician:any;
  ChecksizeRoom!: number;

  ////////////////////////////////////////////
  public auth = getAuth();
  public database = getDatabase(this.firebaseConfig);

  selectedNumroom: any;

  UserList: any = [];
  constructor(private router: Router) {
    this.UserList = [];

    this.gender = [{ gender: 'ผู้ชาย' }, { gender: 'ผู้หญิง' }];

    this.UserList.length = 0;
    const UserRefs = ref(this.database, 'Users/');
    onValue(UserRefs, (snapshot) => {
      snapshot.forEach((childSnapshot) => {
        const Userdata = childSnapshot.exportVal();
        const UserdataVal = childSnapshot.val();

        if (Userdata.role == 'Repairman') {
          this.UserList.push(UserdataVal);
        }
      });
    });
  } //constructor


  ngOnInit(): void {
    
  }

  showRegister() {
    this.displayshowRegister = true;
  }
  showRegistertechnician(){
    this.displayshowRegistertechnician=true;
    
  }
  Registertechnician(){
    
    const Realgender = this.selectedGender.gender;
    set(ref(this.database, 'Users/' + this.inputRegisterID), {

      personalID: this.inputRegisterpersonalID,
      pictureUserUrl: '',
      id: this.inputRegisterID,
      email: this.inputRegisterEmail,
      password: this.inputRegisterPassword,
      firstname: this.inputRegisterFname,
      lastname: this.inputRegisterLname,
      birthday: '',
      gender: Realgender,
      phone: this.inputRegisterPhone,
      role: 'Repairman',
    });
    window.location.reload();

  }

  

  showDetail(userid: string) {
    const useridRef = ref(this.database, 'Users/' + userid);
    onValue(useridRef, (snapshot) => {
      const Usershow = snapshot.val();
      this.UsershowID = Usershow.id;
      this.UsershowEmail = Usershow.email;
      this.UsershowPassword = Usershow.password;
      this.UsershowFname = Usershow.firstname;
      this.UsershowLname = Usershow.lastname;
      this.UsershowBday = Usershow.birthday;
      this.UsershowGender = Usershow.gender;
      this.UsershowPhone = Usershow.phone;
      this.UsershowNumroom = Usershow.numroom;
      this.UsershowPersonalID = Usershow.personalID;
      this.UsershowPictrue = Usershow.pictureUserUrl;
      this.UsershowRole = Usershow.role;
    });

    this.displayshowdetail = true;
  }

  showEdit(userid: string) {
    this.pathtoEditID = userid;
    const useridRef = ref(this.database, 'Users/' + this.pathtoEditID);
    onValue(useridRef, (snapshot) => {
      const Usershow = snapshot.val();
      this.inputEditEmail = Usershow.email;
      this.inputEditPassword = Usershow.password;
      this.inputEditpersonalID = Usershow.personalID;
      this.inputEditID = Usershow.id;
      this.inputEditFname = Usershow.firstname;
      this.inputEditLname = Usershow.lastname;
      this.inputEditBday = Usershow.birthday;
      this.inputEditGender = Usershow.gender;
      this.inputEditPhone = Usershow.phone;
      this.inputEditNumroom = Usershow.numroom;
      this.inputEditPictrue = Usershow.pictureUserUrl;
      this.inputEditRole = Usershow.role;
    });
    this.displayshowEdit = true;
    console.log(this.inputEditGender);
    
  }

  EditUser() {
    console.log(this.inputEditID);
    update(ref(this.database, 'Users/' + this.pathtoEditID+"/"), {
      email: this.inputEditEmail,
      password: this.inputEditPassword,
      personalID: this.inputEditpersonalID,
      id: this.inputEditID,
      firstname: this.inputEditFname,
      lastname: this.inputEditLname,
      birthday: this.inputEditBday,
      gender: this.inputEditGender,
      phone: this.inputEditPhone,
      numroom: this.inputEditNumroom,
      pictureUserUrl: this.inputEditPictrue,
      role: "Repairman",
    });
    this.UserList.length = 0;
    setTimeout(() => {
      window.location.reload();
    }, 2000);
  }

  showDelete(userid: string, numroom: string,nametechnician:string) {
    this.pathtoDeleteUserInroom = numroom;
    this.nametechnician=nametechnician
    this.pathtoDeleteID = userid;
    this.displayshowDelete = true;

    
  }

  DeleteUser() {
    {
      set(ref(this.database, 'Users/' + this.pathtoDeleteID), {});
    }
    this.displayshowDelete = false;
    setTimeout(() => {
      window.location.reload();
    }, 1000);
  }

} ///end
