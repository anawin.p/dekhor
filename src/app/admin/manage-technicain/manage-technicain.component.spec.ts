import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageTechnicainComponent } from './manage-technicain.component';

describe('ManageTechnicainComponent', () => {
  let component: ManageTechnicainComponent;
  let fixture: ComponentFixture<ManageTechnicainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageTechnicainComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageTechnicainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
