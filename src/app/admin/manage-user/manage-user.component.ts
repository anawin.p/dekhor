import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { initializeApp } from 'firebase/app';
import {
  getAuth,
  createUserWithEmailAndPassword,
  deleteUser,
  updateEmail,
  updatePassword,
} from 'firebase/auth';
import firebase from 'firebase/compat/app';

import {
  getDatabase,
  ref,
  onValue,
  set,
  update,
  query,
  orderByChild,
  equalTo,
  remove,
} from 'firebase/database';

interface Gender {
  gender: string;
}

@Component({
  selector: 'app-manage-user',
  templateUrl: './manage-user.component.html',
  styleUrls: ['./manage-user.component.css'],
})
export class ManageUserComponent implements OnInit {
  firebaseConfig = initializeApp({
    apiKey: 'AIzaSyC0GQGYw6w_YvRQCpVrYJ0zU8dnlA9V9jI',
    authDomain: 'dekhordemo-23dde.firebaseapp.com',
    databaseURL:
      'https://dekhordemo-23dde-default-rtdb.asia-southeast1.firebasedatabase.app',
    projectId: 'dekhordemo-23dde',
    storageBucket: 'dekhordemo-23dde.appspot.com',
    messagingSenderId: '535683564882',
    appId: '1:535683564882:web:e419589efe9241723fe68b',
    measurementId: 'G-GCE1HL0W9B',
  });
  displayshowdetail = false;
  displayshowEdit = false;
  displayshowRegister = false;
  displayshowDelete = false;
  displayshowRegistertechnician=false;
  ///////////Register////////////////////

  IsRegister = false;
  Islogin = false;

  inputRegisterID: string = '';
  inputRegisterEmail: string = '';
  inputRegisterPassword: string = '';
  inputRegisterFname: string = '';
  inputRegisterLname: string = '';
  inputRegisterpersonalID: any = '';
  gender!: Gender[];
  selectedGender!: Gender;

  inputRegisterPhone: string = '';

  //////////////////////////////////////////////
  /////////////////Show User data ///////////////////
  UsershowID: string = '';
  UsershowEmail: string = '';
  UsershowPassword: string = '';
  UsershowFname: string = '';
  UsershowLname: string = '';
  UsershowBday: string = '';
  UsershowGender: string = '';
  UsershowPhone: string = '';
  UsershowNumroom: string = '';
  UsershowPersonalID: string = '';
  UsershowPictrue: string = '';
  UsershowRole: string = '';
  Modellenght: any = [];

  ////////////////////////////////////////////
  ///////////////////Edit//////////////////////
  inputEditEmail: string = '';
  inputEditPassword: string = '';
  inputEditpersonalID: string = '';
  inputEditID: string = '';
  inputEditFname: string = '';
  inputEditLname: string = '';
  inputEditBday: string = '';
  inputEditGender: string = '';
  inputEditPhone: string = '';
  inputEditNumroom: string = '';
  inputEditPictrue: string = '';
  inputEditRole: string = '';

  pathtoEditID: string = '';
  //////////////////Register//////////////////
  pathtoRegisterID: any = '';
  pathtoRegisterUserInroom: any = '';
  RegisterP1: any;
  RegisterP2: any;
  RegisterP3: any;
  RegisterTypeRoom: any;
  //////////////////DELETE***/////////////////
  pathtoDeleteID: any = '';
  pathtoDeleteUserInroom: any = '';
  Roomlist: any = [];
  DeleteTypeRoom: any;
  DeleteP1: any;
  DeleteP2: any;
  DeleteP3: any;

  ChecksizeRoom!: number;

  ////////////////////////////////////////////
  public auth = getAuth();
  public database = getDatabase(this.firebaseConfig);

  selectedNumroom: any;

  UserList: any = [];
  constructor(private router: Router) {
    this.UserList = [];

    this.gender = [{ gender: 'ผู้ชาย' }, { gender: 'ผู้หญิง' }];

    this.UserList.length = 0;
    const UserRefs = ref(this.database, 'Users/');
    onValue(UserRefs, (snapshot) => {
      snapshot.forEach((childSnapshot) => {
        const Userdata = childSnapshot.exportVal();
        const UserdataVal = childSnapshot.val();

        if (Userdata.role == 'User') {
          this.UserList.push(UserdataVal);
        }
      });
    });
  } //constructor

  async ngOnInit(): Promise<void> {
    await this.RegisterEmailPassword();
  }
  //ngOnInit

  showRegister() {
    this.displayshowRegister = true;
  }
  

  RegisterEmailPassword() {
    
    const Realgender = this.selectedGender.gender;
    this.UserList.length = 0;
    const numroom = this.inputRegisterID.slice(0, -2);
    this.pathtoRegisterUserInroom = numroom;
    // this.pathtoRegisterID = this.inputRegisterID;
    const str = this.inputRegisterID;
    const last = str.charAt(str.length - 1);
    const IfP1: string = last;

    const RegisterRoomRef = ref(
      this.database,
      'Room/' + this.pathtoRegisterUserInroom
      
    );
    onValue(RegisterRoomRef, (snapshot) => {
      this.ChecksizeRoom = snapshot.size;
      let checksizsing: number = snapshot.size;

      const TypeRoompriceVal = snapshot.val();
      if (checksizsing == 1) {
        // มี 0 คน
        this.RegisterTypeRoom = TypeRoompriceVal.Type;
        // console.log(this.ChecksizeRoom);
      }
      if (checksizsing == 2) {
        // มี 1 คน
        this.RegisterP1 = TypeRoompriceVal[1];
        this.RegisterTypeRoom = TypeRoompriceVal.Type;
        // console.log(this.ChecksizeRoom);
      }

      if (checksizsing == 3) {
        // มี 2 คน
        this.RegisterP1 = TypeRoompriceVal[1];
        this.RegisterP2 = TypeRoompriceVal[2];
        this.RegisterTypeRoom = TypeRoompriceVal.Type;
        // console.log(this.ChecksizeRoom);
      }
      if (checksizsing == 4) {
        // มี 3 คน
        this.RegisterP1 = TypeRoompriceVal[1];
        this.RegisterP2 = TypeRoompriceVal[2];
        this.RegisterP3 = TypeRoompriceVal[3];
        this.RegisterTypeRoom = TypeRoompriceVal.Type;
        // console.log(this.ChecksizeRoom);
      }
    });

    if (
      this.inputRegisterID == null ||
      this.inputRegisterID == '' ||
      this.inputRegisterID == ' '
    ) {
      return;
    } else {
      set(ref(this.database, 'Users/' + this.inputRegisterID), {
        numroom: numroom,
        personalID: this.inputRegisterpersonalID,
        pictureUserUrl: '',
        id: this.inputRegisterID,
        email: this.inputRegisterEmail,
        password: this.inputRegisterPassword,
        firstname: this.inputRegisterFname,
        lastname: this.inputRegisterLname,
        birthday: '',
        gender: Realgender,
        phone: this.inputRegisterPhone,
        role: 'User',
      });
      ///////////
      this.Modellenght.length = 0;
      /////////
    }

    return new Promise<void>((resolve) => {
      setTimeout(() => {
        if (this.ChecksizeRoom == 1 && IfP1 == '1') {
          // ถ้าไม่มีคน เป็นที่ 1
          set(ref(this.database, 'Room/' + this.pathtoRegisterUserInroom), {
            1: this.inputRegisterID,
            Type: this.RegisterTypeRoom,
          });
        }
        if (this.ChecksizeRoom == 1 && IfP1 == '2') {
          // ถ้าไม่มีคน เป็นที่ 2
          set(ref(this.database, 'Room/' + this.pathtoRegisterUserInroom), {
            2: this.inputRegisterID,
            Type: this.RegisterTypeRoom,
          });
        }
        if (this.ChecksizeRoom == 1 && IfP1 == '3') {
          // ถ้าไม่มีคน เป็นที่ 3
          set(ref(this.database, 'Room/' + this.pathtoRegisterUserInroom), {
            3: this.inputRegisterID,
            Type: this.RegisterTypeRoom,
          });
        }
        if (this.ChecksizeRoom == 2 && IfP1 == '1') {
          // ถ้ามี 1 คน เป็นที่ 1
          set(ref(this.database, 'Room/' + this.pathtoRegisterUserInroom), {
            1: this.inputRegisterID,
            2: this.RegisterP1,
            Type: this.RegisterTypeRoom,
          });
        }
        if (this.ChecksizeRoom == 2 && IfP1 == '2') {
          // ถ้ามี 1 คน เป็นที่ 2
          set(ref(this.database, 'Room/' + this.pathtoRegisterUserInroom), {
            1: this.RegisterP1,
            2: this.inputRegisterID,
            Type: this.RegisterTypeRoom,
          });
        }
        if (this.ChecksizeRoom == 2 && IfP1 == '3') {
          // ถ้ามี 1 คน เป็นที่ 3
          set(ref(this.database, 'Room/' + this.pathtoRegisterUserInroom), {
            1: this.RegisterP1,
            3: this.inputRegisterID,
            Type: this.RegisterTypeRoom,
          });
        }

        if (this.ChecksizeRoom == 3 && IfP1 == '1') {
          // ถ้ามี 2 คน  เป็นที่ 1
          set(ref(this.database, 'Room/' + this.pathtoRegisterUserInroom), {
            1: this.inputRegisterID,
            2: this.RegisterP1,
            3: this.RegisterP2,
            Type: this.RegisterTypeRoom,
          });
        }
        if (this.ChecksizeRoom == 3 && IfP1 == '2') {
          window.alert(this.pathtoRegisterUserInroom)
          // ถ้ามี 2 คน  เป็นที่ 2
          set(ref(this.database, 'Room/' + this.pathtoRegisterUserInroom), {
            Type: this.RegisterTypeRoom,
            1: this.RegisterP1,
            2: this.inputRegisterID,
            3: this.RegisterP2,
          });
        }
        if (this.ChecksizeRoom == 3 && IfP1 == '3') {
          // ถ้ามี 2 คน  เป็นที่ 3
          set(ref(this.database, 'Room/' + this.pathtoRegisterUserInroom), {
            Type: this.RegisterTypeRoom,
            1: this.RegisterP1,
            2: this.RegisterP2,
            3: this.inputRegisterID,
          });
        }
        window.location.reload();
        resolve();
      }, 200);
    });
   
  }

  showDetail(userid: string) {
    const useridRef = ref(this.database, 'Users/' + userid);
    onValue(useridRef, (snapshot) => {
      const Usershow = snapshot.val();
      this.UsershowID = Usershow.id;
      this.UsershowEmail = Usershow.email;
      this.UsershowPassword = Usershow.password;
      this.UsershowFname = Usershow.firstname;
      this.UsershowLname = Usershow.lastname;
      this.UsershowBday = Usershow.birthday;
      this.UsershowGender = Usershow.gender;
      this.UsershowPhone = Usershow.phone;
      this.UsershowNumroom = Usershow.numroom;
      this.UsershowPersonalID = Usershow.personalID;
      this.UsershowPictrue = Usershow.pictureUserUrl;
      this.UsershowRole = Usershow.role;
    });

    this.displayshowdetail = true;
  }

  showEdit(userid: string) {
    this.pathtoEditID = userid;
    const useridRef = ref(this.database, 'Users/' + this.pathtoEditID);
    onValue(useridRef, (snapshot) => {
      const Usershow = snapshot.val();
      this.inputEditEmail = Usershow.email;
      this.inputEditPassword = Usershow.password;
      this.inputEditpersonalID = Usershow.personalID;
      this.inputEditID = Usershow.id;
      this.inputEditFname = Usershow.firstname;
      this.inputEditLname = Usershow.lastname;
      this.inputEditBday = Usershow.birthday;
      this.inputEditGender = Usershow.gender;
      this.inputEditPhone = Usershow.phone;
      this.inputEditNumroom = Usershow.numroom;
      this.inputEditPictrue = Usershow.pictureUserUrl;
      this.inputEditRole = Usershow.role;
    });
    this.displayshowEdit = true;
  }

  EditUser() {
    console.log(this.inputEditID);
    update(ref(this.database, 'Users/' + this.pathtoEditID+"/"), {
      email: this.inputEditEmail,
      password: this.inputEditPassword,
      personalID: this.inputEditpersonalID,
      id: this.inputEditID,
      firstname: this.inputEditFname,
      lastname: this.inputEditLname,
      birthday: this.inputEditBday,
      gender: this.inputEditGender,
      phone: this.inputEditPhone,
      numroom: this.inputEditNumroom,
      pictureUserUrl: this.inputEditPictrue,
      role: this.inputEditRole,
    });
    this.UserList.length = 0;
    setTimeout(() => {
      window.location.reload();
    }, 2000);
  }

  showDelete(userid: string, numroom: string) {
    this.pathtoDeleteUserInroom = numroom;

    this.pathtoDeleteID = userid;
    this.displayshowDelete = true;

    const DeleteRoomRef = ref(
      this.database,
      'Room/' + this.pathtoDeleteUserInroom
    );
    onValue(DeleteRoomRef, (snapshot) => {
      this.ChecksizeRoom = snapshot.size;
      const TypeRoompriceVal = snapshot.exportVal();
      if (this.ChecksizeRoom == 1) {
        // มี 0 คน
        this.DeleteTypeRoom = TypeRoompriceVal.Type;
      }
      if (this.ChecksizeRoom == 2) {
        // มี 1 คน
        this.DeleteP1 = TypeRoompriceVal[1];
        this.DeleteTypeRoom = TypeRoompriceVal.Type;
      }

      if (this.ChecksizeRoom == 3) {
        // มี 2 คน
        this.DeleteP1 = TypeRoompriceVal[1];
        this.DeleteP2 = TypeRoompriceVal[2];
        this.DeleteTypeRoom = TypeRoompriceVal.Type;
      }
      if (this.ChecksizeRoom == 4) {
        // มี 3 คน
        this.DeleteP1 = TypeRoompriceVal[1];
        this.DeleteP2 = TypeRoompriceVal[2];
        this.DeleteP3 = TypeRoompriceVal[3];
        this.DeleteTypeRoom = TypeRoompriceVal.Type;
      }
    });
    // console.log(this.ChecksizeRoom);
  }

  DeleteUser() {
    const str = this.pathtoDeleteID;
    const last = str.charAt(str.length - 1);
    const IfP1: string = last;

    if (this.ChecksizeRoom == 2 && IfP1 == '1') {
      // ถ้ามี 1 คน เป็นที่ 1
      set(ref(this.database, 'Room/' + this.pathtoDeleteUserInroom), {
        Type: this.DeleteTypeRoom,
      });
    }
    if (this.ChecksizeRoom == 2 && IfP1 == '2') {
      // ถ้ามี 1 คน เป็นที่ 2
      set(ref(this.database, 'Room/' + this.pathtoDeleteUserInroom), {
        Type: this.DeleteTypeRoom,
      });
    }
    if (this.ChecksizeRoom == 2 && IfP1 == '3') {
      // ถ้ามี 1 คน เป็นที่ 3
      set(ref(this.database, 'Room/' + this.pathtoDeleteUserInroom), {
        Type: this.DeleteTypeRoom,
      });
    }
    if (this.ChecksizeRoom == 3 && IfP1 == '1') {
      // ถ้ามี 2 คน  เป็นที่ 1
      set(ref(this.database, 'Room/' + this.pathtoDeleteUserInroom), {
        Type: this.DeleteTypeRoom,
        2: this.DeleteP1,
      });
    }
    if (this.ChecksizeRoom == 3 && IfP1 == '2') {
      // ถ้ามี 2 คน  เป็นที่ 2
      set(ref(this.database, 'Room/' + this.pathtoDeleteUserInroom), {
        Type: this.DeleteTypeRoom,
        1: this.DeleteP1,
      });
    }
    if (this.ChecksizeRoom == 4 && IfP1 == '1') {
      // ถ้ามี 3 คน เป็นที่ 1
      set(ref(this.database, 'Room/' + this.pathtoDeleteUserInroom), {
        Type: this.DeleteTypeRoom,
        2: this.DeleteP2,
        3: this.DeleteP3,
      });
    }
    if (this.ChecksizeRoom == 4 && IfP1 == '2') {
      // ถ้ามี 3 คน เป็นที่ 2
      set(ref(this.database, 'Room/' + this.pathtoDeleteUserInroom), {
        Type: this.DeleteTypeRoom,
        1: this.DeleteP1,
        3: this.DeleteP3,
      });
    }
    if (this.ChecksizeRoom == 4 && IfP1 == '3') {
      // ถ้ามี 3 คน เป็นที่ 3
      set(ref(this.database, 'Room/' + this.pathtoDeleteUserInroom), {
        Type: this.DeleteTypeRoom,
        1: this.DeleteP1,
        2: this.DeleteP2,
      });
    }
    if (
      this.pathtoDeleteID == '' &&
      this.pathtoDeleteID == null &&
      this.pathtoDeleteID == ''
    ) {
      console.log(this.pathtoDeleteID);
      return;
    } else {
      set(ref(this.database, 'Users/' + this.pathtoDeleteID), {});
    }

    this.displayshowDelete = false;
    setTimeout(() => {
      window.location.reload();
    }, 1000);
  }

} ///end
