import { Component } from '@angular/core';
import { initializeApp } from 'firebase/app';
import { getDatabase, ref, onValue } from 'firebase/database';
import { PrimeNGConfig } from 'primeng/api';
import dateFormat from 'dateformat';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css'],
})
export class InvoiceComponent {
  firebaseConfig = initializeApp({
    apiKey: 'AIzaSyC0GQGYw6w_YvRQCpVrYJ0zU8dnlA9V9jI',
    authDomain: 'dekhordemo-23dde.firebaseapp.com',
    databaseURL:
      'https://dekhordemo-23dde-default-rtdb.asia-southeast1.firebasedatabase.app',
    projectId: 'dekhordemo-23dde',
    storageBucket: 'dekhordemo-23dde.appspot.com',
    messagingSenderId: '535683564882',
    appId: '1:535683564882:web:e419589efe9241723fe68b',
    measurementId: 'G-GCE1HL0W9B',
  });
  public database = getDatabase(this.firebaseConfig);

  loading = [false, false, false, false];

  date9!: Date;
  now = new Date();
  modelCalenda?: string;
  Realmonth?: number;
  D = new Date();
  Month = this.D.getMonth();
  Year = this.D.getFullYear();
Year$?:any;
Month$?:any;
  stateOptions: any = [];
  value2!: number;

  displayBasic = false;
  displayModal?:boolean;
  RoomPrice?: string;
  electricity?:string;
  elecafter?: string;
  elecbefore?: string;
  water?: string;
  waterafter?: string;
  waterbefore?: string;
  SumPrice?: string;
  WaterPrice?: string;
  ElecPrice?: string;
  Numroom?:string;
  lastwater?:any;
  lastelec?:any;

  Roomkey: any = [];
RoomBillList:any=[];

  constructor(private primeNGConfig: PrimeNGConfig) {
    const CostRefs = ref(
      this.database,
      'Bills/' + this.Year + '/' + this.Month + '/'
    );

    onValue(CostRefs, (snapshot) => {
      snapshot.forEach((childSnapshot) => {
        // this.MessageList = childSnapshot.key;
        const Roomkeydata = childSnapshot.key;
        const RoomBilldata = childSnapshot.exportVal();

        this.Roomkey.push(Roomkeydata);
        this.RoomBillList.push(RoomBilldata);
        
        
      });
    });
console.log(this.RoomBillList);

    this.primeNGConfig.ripple = true;
    this.stateOptions = [{ name: 'ปรับ', value: 1 }];

    ///////////////TEST สรุป/////////////////////////////////
    // const BillsRef = ref(this.database, 'Bills/2022/0/');
    // onValue(BillsRef, (snapshot) => {
    //   snapshot.forEach((childSnapshot) => {
    //     const Billdata = childSnapshot.val();
    //     this.Bills.push(Billdata.sum);
    //     this.Roomprice = this.Bills.map((i: any) => Number(i));

    //     var a = this.Roomprice;
    //     this.Roompricezz = a.reduce(function (a: any, b: any) {
    //       return a + b;
    //     }, 0);
    //     console.log(this.Roompricezz);
    //   });
    // });
    ///////////////////////////////////////////////////////////////////
  }
  showNewBills(numroom: string) {
    this.Numroom = numroom;
    this.displayBasic = true;
    const BillRefs = ref(this.database,
      'Bills/' + this.Year$ + '/' + this.Month$ + '/'
    );
    onValue(BillRefs, (snapshot) => {
      snapshot.forEach((childSnapshot) => {
        const billdata = childSnapshot.exportVal();
        this.RoomPrice=billdata.roomprice;
        this.electricity=billdata.electricity;
        this.elecafter=billdata.elecafter;
        this.elecbefore=billdata.elecbefore;
        this.water=billdata.water;
        this.waterafter=billdata.waterafter;
        this.waterbefore=billdata.waterbefore;
        this.SumPrice=billdata.sum
        this.lastelec=billdata.elecafter-billdata.elecbefore
        this.lastwater=billdata.waterafter-billdata.waterbefore
      });
    });
    
  }
  load(index: any) {

   let Year = this.modelCalenda = dateFormat(this.date9, 'yyyy');
   let NMonth = this.modelCalenda = dateFormat(this.date9, 'm');
   let RealMonth = parseFloat(NMonth)-1;
this.Year$ = Year;
this.Month$=RealMonth;
    this.Roomkey.length = 0;
  

    this.loading[index] = true;
    setTimeout(() => (this.loading[index] = false), 800);
    const CostRefs = ref(
      this.database,
      'Bills/' + Year + '/' + RealMonth + '/'
    );

    setTimeout(
      () =>
        onValue(CostRefs, (snapshot) => {
          snapshot.forEach((childSnapshot) => {
            // this.MessageList = childSnapshot.key;
            const Userdata = childSnapshot.key;
            this.Roomkey.push(Userdata);
          });
        }),
      800
    );
  }
  onsavebill(){
    
  }

  

}
