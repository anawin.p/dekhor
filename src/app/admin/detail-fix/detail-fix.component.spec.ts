import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailFixComponent } from './detail-fix.component';

describe('DetailFixComponent', () => {
  let component: DetailFixComponent;
  let fixture: ComponentFixture<DetailFixComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailFixComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailFixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
