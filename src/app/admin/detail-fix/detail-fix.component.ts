import { Component, Input, OnInit } from '@angular/core';
import { initializeApp } from 'firebase/app';
import { ref, onValue, getDatabase, set, update } from 'firebase/database';
import { FileUploadContractService } from 'src/app/services/file-upload-contract.service';
import { FileUploadContract } from 'src/app/models/file-upload-contract';
import {MessageService,PrimeNGConfig} from 'primeng/api';
import firebase from '@firebase/app';
require('firebase/auth');
interface Floor {
  name: string;
}
interface UserModel {
  firstname: string;
  lastname: string;
}

@Component({
  selector: 'app-detail-fix',
  templateUrl: './detail-fix.component.html',
  styleUrls: ['./detail-fix.component.css'], 
  providers: [MessageService]
})
export class DetailFixComponent implements OnInit {
  selectedFiles?: FileList;
  currentFileUpload?: FileUploadContract;
  percentage = 0;

  firebaseConfig = initializeApp({
    apiKey: 'AIzaSyC0GQGYw6w_YvRQCpVrYJ0zU8dnlA9V9jI',
    authDomain: 'dekhordemo-23dde.firebaseapp.com',
    databaseURL:
      'https://dekhordemo-23dde-default-rtdb.asia-southeast1.firebasedatabase.app',
    projectId: 'dekhordemo-23dde',
    storageBucket: 'dekhordemo-23dde.appspot.com',
    messagingSenderId: '535683564882',
    appId: '1:535683564882:web:e419589efe9241723fe68b',
    measurementId: 'G-GCE1HL0W9B',
  });
  rooms: any = '';
  Roomlist: any = [];
  displayshowNewrooms = false;
  displayshowroom = false;
  firstnamelist: any = [];
  lastnamelist: any = [];
  namelist: any = [];
  codenamelist: any = [];
  selectroom: any = '';
  UserViewlist: UserModel[] = [];

  inputNumroom: any = '';

  public database = getDatabase(this.firebaseConfig);
  constructor(private uploadcontractService: FileUploadContractService,private messageService: MessageService,private primengConfig: PrimeNGConfig) {}

  ngOnInit(): void {
    this.primengConfig.ripple = true;
    this.Roomlist.length = 0;
    const UserRefs = ref(this.database, 'Room');
    onValue(UserRefs, (snapshot) => {
      snapshot.forEach((childSnapshot) => {
        const Userdata = childSnapshot.key;

        this.Roomlist.push(Userdata);
      });
    });
    
  }
  selectFile(event: any): void {
    this.selectedFiles = event.target.files;
  }
  upload(roomName:String): void {
    if (this.selectedFiles) {
      const file: File | null = this.selectedFiles.item(0);
      this.selectedFiles = undefined;
      if (file) {
      
        this.currentFileUpload = new FileUploadContract(file);
        this.uploadcontractService.roomName = roomName;
        this.uploadcontractService
          .pushFileToStorage(this.currentFileUpload)
          .subscribe(
            (percentage) => {
              this.percentage = Math.round(percentage ? percentage : 0);
              
            },
            (error) => {
              console.log(error);
            }
            
          );
          setTimeout(() => {
            this.messageService.add({severity:'success', summary: 'อัปโหลดสำเร็จ'});
            
          }, 2000);
          setTimeout(() => {
            
            window.location.reload();
          }, 3000);
          
      }
    }
  }
  showNewrooms() {
    this.displayshowNewrooms = true;
  }

  showRoom(numroom: string) {
    this.rooms = numroom;
    this.codenamelist.length = 0;
    this.UserViewlist.length = 0;
    this.namelist.length = 0;

    const RoomRefs = ref(this.database, 'Room/' + numroom);
    onValue(RoomRefs, (snapshot) => {
      snapshot.forEach((childSnapshot) => {
        const Userdata = childSnapshot.val();
        this.codenamelist.push(Userdata);
      });
    });
    console.log(this.codenamelist);

    
    this.displayshowroom = true;
  }

}
