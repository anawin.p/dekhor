import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin/admin.component';

import { HomeLoginComponent } from './home-login/home-login.component';

import { SuperadminComponent } from './superadmin/superadmin.component';


const routes: Routes = [
  { path: '', redirectTo: '/welcome', pathMatch: 'full' },

  { path: 'welcome', component: HomeLoginComponent },

  { path: 'superadmin', component: SuperadminComponent },

  { path: 'admin', component: AdminComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
