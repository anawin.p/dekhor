import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { initializeApp } from 'firebase/app';
import {
  getAuth,
  onAuthStateChanged,
  signInWithEmailAndPassword,
  signOut,
  updateEmail,
} from 'firebase/auth';
import { getDatabase, onValue, push, ref, update } from 'firebase/database';
import { ConfirmationService, MessageService, } from 'primeng/api';

@Component({
  selector: 'app-home-login',
  templateUrl: './home-login.component.html',
  styleUrls: ['./home-login.component.css'],
})
export class HomeLoginComponent implements OnInit {
  statuspage: number = 0;

  firebaseConfig = initializeApp({
    apiKey: 'AIzaSyC0GQGYw6w_YvRQCpVrYJ0zU8dnlA9V9jI',
    authDomain: 'dekhordemo-23dde.firebaseapp.com',
    databaseURL:
      'https://dekhordemo-23dde-default-rtdb.asia-southeast1.firebasedatabase.app',
    projectId: 'dekhordemo-23dde',
    storageBucket: 'dekhordemo-23dde.appspot.com',
    messagingSenderId: '535683564882',
    appId: '1:535683564882:web:e419589efe9241723fe68b',
    measurementId: 'G-GCE1HL0W9B',
  });

  public database = getDatabase(this.firebaseConfig);
  Islogin = false;

  ID: any = '';

  inputLoginEmail: string = '';
  inputLoginPassword: string = '';
  ////////////////// Route //////////////////

  /////////////  โชว์ Promotion   ////////////
  Pictureurl?: string;
  displayPromotion = false;
  displayRoomsuite = false;
  displayRoomRegular = false;
  displayshowLogin = false;
  Promotion = '';
  Roomsuite = '';
  RoomRegular = '';
  AddressHor = 'ต.คลองหก อ.คลองหลวง ปทุมธานี';
  Showtop =
    'สัมผัสอพาร์เมนท์รูปแบบใหม่ ห้องพักสไตล์หรูหราแบบทันสมัย ทำเลดี ติดรถมอไซต์ วินบริการเยอะ ใกล้มหาลัยแต่เดินอีกนิดหน่อย';
  TypeRoomprice: any = [];
  RoomNormalList: any = [];
  RoomSuiteList: any = [];
  electricitybill: any = '';
  WaterPrice: any = '';
  images: any=[ 
    {

      "previewImageSrc": "https://pix8.agoda.net/hotelImages/149/149146/149146_15092115060036231785.jpg?ca=5&ce=1&s=1024x768",
      "thumbnailImageSrc": "https://pix8.agoda.net/hotelImages/149/149146/149146_15092115060036231785.jpg?ca=5&ce=1&s=1024x768",
      "alt": "Description for Image 1",
      "title": "Title 1"

  },


];
    responsiveOptions:any[] = [
      {
          breakpoint: '1024px',
          numVisible: 5
      },
      {
          breakpoint: '768px',
          numVisible: 3
      },
      {
          breakpoint: '560px',
          numVisible: 1
      } 
  ];
  
  /////////////  โชว์ Promotion   ////////////
  role = '';
  userid = '';

  constructor(
    private router: Router,
    private confirmationService: ConfirmationService,
    private messageService: MessageService,

    
  ) {
    
    const auth = getAuth();

    const PicRef = ref(this.database, 'View/RoomRegular/Photo');
    onValue(PicRef, (snapshot) => {
      const PicRefdata = snapshot.exportVal();
      //this.Pictureurl = PicRefdata.url;
      this.images.push(PicRefdata)
      console.log(this.images);
      
      
    });

    onAuthStateChanged(auth, (user) => {
      this.ID = this.inputLoginEmail.slice(0, -11);
      if (user) {
        console.log(this.userid);
        this.userid = user.uid;
        const roleRef = ref(this.database, 'Users/' + this.ID);
        onValue(roleRef, (snapshot) => {
          const roleRefs = snapshot.val();
          if (roleRefs.role === null) {
            signOut(auth)
              .then(() => {
                this.router.navigateByUrl('/welcome');
                console.log('ออกจากระบบเรียบร้อย.');
              })
              .catch((error) => {
                console.log(error);
              });
          } else if (roleRefs.role === 'Admin') {
            this.Islogin = true;
            this.router.navigateByUrl('/admin');
          } else if (roleRefs.role === 'Superadmin') {
            this.Islogin = true;
            this.router.navigateByUrl('/superadmin');
          }
        });
      } else if (!user) {
        console.log('ยังไม่ได้ Login');
        this.router.navigateByUrl('/welcome');
      }
    });
    const PromotionRef = ref(this.database, 'View/' + 'Promotion');
    onValue(PromotionRef, (snapshot) => {
      const Promotions = snapshot.val();

      this.Promotion = Promotions.Promotion;
    });
  } //constructor

  ngOnInit(): void {
    
    //////////////////////ทำหน้า Route //////////////////////
    sessionStorage.setItem('statuspage', `${this.statuspage}`);
    //////////////////////////////////////////////////////////////
    const ElecRefs = ref(this.database, 'View/electricitybill');
    onValue(ElecRefs, (snapshot) => {
      const Billdata = snapshot.val();
      this.electricitybill = Billdata.Elec;
      this.WaterPrice = Billdata.Water;
    });

    const RoomSuiteRefs = ref(this.database, 'Room/');
    onValue(RoomSuiteRefs, (snapshot) => {
      snapshot.forEach((childSnapshot) => {
        const Userdata = childSnapshot.exportVal();
        const UserdataVal = childSnapshot.val();
        const SizeData = childSnapshot.size;
        if (Userdata.Type == 'Suite' && SizeData == 1) {
          this.RoomSuiteList.push(UserdataVal);
        }
      });
    });
    const RoomNormalRefs = ref(this.database, 'Room/');
    onValue(RoomNormalRefs, (snapshot) => {
      snapshot.forEach((childSnapshot) => {
        const Userdata = childSnapshot.exportVal();
        const UserdataVal = childSnapshot.val();
        const SizeData = childSnapshot.size;
        if (Userdata.Type == 'Normal' && SizeData == 1) {
          this.RoomNormalList.push(UserdataVal);
        }
      });
    });
    const TypeRoomprice = ref(this.database, 'TypeRooms');
    onValue(TypeRoomprice, (snapshot) => {
      snapshot.forEach((childSnapshot) => {
        const TypeRoompriceVal = childSnapshot.val();
        this.TypeRoomprice.push(TypeRoompriceVal);
      });
    });
  }

  showLogin() {
    this.displayshowLogin = true;
  }

  LoginEmailPassword() {
    const ID = this.inputLoginEmail.slice(0, -11);
    const auth = getAuth();
    const current = new Date();
    const timestamp = current.getTime();
    signInWithEmailAndPassword(
      auth,
      this.inputLoginEmail,
      this.inputLoginPassword
    )
      .then((userCredential) => {
        // Signed in
        const user = userCredential.user;

        console.log(user);

        update(ref(this.database, 'Users/' + ID), {
          Last_login: timestamp,
        });
        this.displayshowLogin = false;
      })
      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
        if (errorCode === 'auth/invalid-email') {
          alert('กรุณากรอกอีเมล์ของท่าน.');
          console.log(errorCode);
        }

        if (errorCode === 'auth/internal-error') {
          alert('กรุณากรอกรหัสผ่านของท่าน.');
          console.log(errorCode);
        }
        if (errorCode === 'auth/wrong-password') {
          alert('รหัสผ่านของท่านไม่ถูกต้อง.');
          console.log(errorCode);
        }
        if (errorCode === 'auth/user-not-found') {
          alert('ไม่มีUserนี้ในระบบ.');
          console.log(errorCode);
        }
      });
  }

  handleLogout() {
    this.Islogin = false;
  }

  showPromotion() {
    console.log(this.Promotion);
    this.displayPromotion = true;
    const PromotionRef = ref(this.database, 'View/' + 'Promotion');
    onValue(PromotionRef, (snapshot) => {
      const Promotions = snapshot.val();

      this.Promotion = Promotions.Promotion;
    });
  }

  showRoomsuite() {
    this.displayRoomsuite = true;

    const RoomsuiteRef = ref(this.database, 'View/' + 'Roomsuite');
    onValue(RoomsuiteRef, (snapshot) => {
      const Roomsuites = snapshot.val();

      this.Roomsuite = Roomsuites.Roomsuite;
    });
  }

  showRoomRegular() {
    console.log(this.RoomRegular);

    this.displayRoomRegular = true;

    const RoomRegularRef = ref(this.database, 'View/' + 'RoomRegular');
    onValue(RoomRegularRef, (snapshot) => {
      const RoomRegulars = snapshot.val();

      this.RoomRegular = RoomRegulars.RoomRegular;
    });
  }
}
