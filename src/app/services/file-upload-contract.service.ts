import { Injectable } from '@angular/core';
import {
  AngularFireDatabase,
  AngularFireList,
} from '@angular/fire/compat/database';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { FileUploadContract } from '../models/file-upload-contract';
import { FileUpload } from '../models/file-upload.model';

@Injectable({
  providedIn: 'root',
})
export class FileUploadContractService {
  private basePath = '/Contract';
   roomName: String = "";
  constructor(
    private db: AngularFireDatabase,
    private storage: AngularFireStorage
  ) {}
  pushFileToStorage(fileUpload: FileUpload): Observable<number | undefined> {
    // const filePath = `${this.basePath}/${fileUpload.file.name}`;
    const filePath = `${this.basePath}/${this.roomName}`;
    const storageRef = this.storage.ref(filePath);
    console.log(filePath);
    
    
    const uploadTask = this.storage.upload(filePath, fileUpload.file);
    uploadTask
      .snapshotChanges()
      .pipe(
        finalize(() => {
          storageRef.getDownloadURL().subscribe((downloadURL) => {
            fileUpload.url = downloadURL;
            // fileUpload.name = fileUpload.file.name.slice(0, 3)
            console.log(fileUpload);
            fileUpload.name = filePath.slice(-3);
            this.saveFileData(fileUpload.url, fileUpload);
          });
        })
      )
      .subscribe();
    return uploadTask.percentageChanges();
  }
  private saveFileData(path:string,fileUpload: FileUpload): void {
    this.db.list(this.basePath).push(fileUpload);
  }
  getFiles(numberItems: number): AngularFireList<FileUpload> {
    return this.db.list(this.basePath, (ref) => ref.limitToLast(numberItems));
  }
  deleteFile(fileUpload: FileUpload): void {
    this.deleteFileDatabase(fileUpload.key)
      .then(() => {
        this.deleteFileStorage(fileUpload.name);
      })
      .catch((error) => console.log(error));
  }
  private deleteFileDatabase(key: string): Promise<void> {
    return this.db.list(this.basePath).remove(key);
  }
  private deleteFileStorage(name: string): void {
    const storageRef = this.storage.ref(this.basePath);
    storageRef.child(name).delete();
  }
}