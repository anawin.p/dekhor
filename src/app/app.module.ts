import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AdminComponent } from './admin/admin.component';
import { SuperadminComponent } from './superadmin/superadmin.component';
import { HomeLoginComponent } from './home-login/home-login.component';
import { ChatComponent } from './chat/chat.component';
import { FormsModule } from '@angular/forms';



import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularFireModule } from '@angular/fire/compat/';
import { AngularFireDatabaseModule } from '@angular/fire/compat/database';
import { AngularFireStorageModule } from '@angular/fire/compat/storage';


import firebase from '@firebase/app';
require('firebase/auth');
export const config = {
  apiKey: 'AIzaSyC0GQGYw6w_YvRQCpVrYJ0zU8dnlA9V9jI',
  authDomain: 'dekhordemo-23dde.firebaseapp.com',
  databaseURL:
    'https://dekhordemo-23dde-default-rtdb.asia-southeast1.firebasedatabase.app',
  projectId: 'dekhordemo-23dde',
  storageBucket: 'dekhordemo-23dde.appspot.com',
  messagingSenderId: '535683564882',
  appId: '1:535683564882:web:e419589efe9241723fe68b',
  measurementId: 'G-GCE1HL0W9B',
};

// --------------------------------------NG prime---------------------------------------------------
import {ConfirmPopupModule} from 'primeng/confirmpopup';

import {GalleriaModule} from 'primeng/galleria';
import { ConfirmationService, MessageService } from 'primeng/api';
import { InputTextModule } from 'primeng/inputtext';
import { PasswordModule } from 'primeng/password';
import { ButtonModule } from 'primeng/button';
// import { environment } from 'src/environments/environment';
import { TableModule } from 'primeng/table';
/////////////////////////////////////////////////////////////////////////
import { ToastModule } from 'primeng/toast';
import { CalendarModule } from 'primeng/calendar';
import { SliderModule } from 'primeng/slider';
import { MultiSelectModule } from 'primeng/multiselect';
import { ContextMenuModule } from 'primeng/contextmenu';
import { DialogModule } from 'primeng/dialog';

import { DropdownModule } from 'primeng/dropdown';
import { ProgressBarModule } from 'primeng/progressbar';
import { DynamicDialogModule } from 'primeng/dynamicdialog';

import { FileUploadModule } from 'primeng/fileupload';
import { ToolbarModule } from 'primeng/toolbar';
import { RatingModule } from 'primeng/rating';
import { RadioButtonModule } from 'primeng/radiobutton';
import { InputNumberModule } from 'primeng/inputnumber';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { CheckboxModule } from 'primeng/checkbox';

import { InputTextareaModule } from 'primeng/inputtextarea';

import { ImageModule } from 'primeng/image';
import { AccordionModule } from 'primeng/accordion';
import { EditInformationComponent } from './superadmin/edit-information/edit-information.component';
import { ManageUsersComponent } from './superadmin/manage-users/manage-users.component';
import { MenubarModule } from 'primeng/menubar';
import { RoomComponent } from './admin/room/room.component';
import { DetailFixComponent } from './admin/detail-fix/detail-fix.component';
import { InvoiceComponent } from './admin/invoice/invoice.component';
import { ManageUserComponent } from './admin/manage-user/manage-user.component';
import { CostutilitiesComponent } from './admin/costutilities/costutilities.component';
import { SelectButtonModule } from 'primeng/selectbutton';
import { HttpClientModule } from '@angular/common/http';
import { UploadListComponent } from './superadmin/edit-information/components/upload-list/upload-list.component';
import { UploadDetailsComponent } from './superadmin/edit-information/components/upload-details/upload-details.component';
import { SummaryComponent } from './admin/summary/summary.component';
import { initializeApp,provideFirebaseApp } from '@angular/fire/app';
import { environment } from '../environments/environment';
import { provideDatabase,getDatabase } from '@angular/fire/database';
import { provideMessaging,getMessaging } from '@angular/fire/messaging';
import { provideStorage,getStorage } from '@angular/fire/storage';
import {TabViewModule} from 'primeng/tabview';
import {CardModule} from 'primeng/card';
import { ManageTechnicainComponent } from './admin/manage-technicain/manage-technicain.component';

// --------------------------------------NG prime---------------------------------------------------

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    SuperadminComponent,
    HomeLoginComponent,
    ChatComponent,
    EditInformationComponent,
    ManageUsersComponent,
    RoomComponent,
    DetailFixComponent,
    InvoiceComponent,
    ManageUserComponent,
    CostutilitiesComponent,
    SummaryComponent,
    ManageTechnicainComponent,
    

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(config),
    AngularFireDatabaseModule,
    AngularFireStorageModule,
    ///////PrimeNG///////
    HttpClientModule,
    PasswordModule,
    InputTextModule,
    ButtonModule,
    TableModule,
    CheckboxModule,
    CalendarModule,
    SliderModule,
    DialogModule,
    MultiSelectModule,
    ContextMenuModule,
    DropdownModule,
    ButtonModule,
    ToastModule,
    InputTextModule,
    ProgressBarModule,
    FileUploadModule,
    ToolbarModule,
    RatingModule,
    FormsModule,
    RadioButtonModule,
    InputNumberModule,
    ConfirmDialogModule,
    InputTextareaModule,
    BrowserAnimationsModule,
    DynamicDialogModule,
    ImageModule,
    AccordionModule,
    MenubarModule,
    FormsModule,
    SelectButtonModule,
    provideFirebaseApp(() => initializeApp(environment.firebase)),
    provideDatabase(() => getDatabase()),
    provideMessaging(() => getMessaging()),
    provideStorage(() => getStorage()),
    GalleriaModule,
    TabViewModule,
    CardModule,
    ConfirmPopupModule,
    

    ///////PrimeNG///////
  ],
  providers: [MessageService, ConfirmationService],
  bootstrap: [AppComponent],
  exports: [
    AdminComponent,
    SuperadminComponent,
    HomeLoginComponent,
    ChatComponent,
    EditInformationComponent,
    ManageUsersComponent,
    RoomComponent,
    DetailFixComponent,
    InvoiceComponent,
    ManageUserComponent,
    CostutilitiesComponent,
    SummaryComponent,
    ManageTechnicainComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule {}
