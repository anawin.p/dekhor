import { TestBed } from '@angular/core/testing';

import { FileUploadContractService } from './file-upload-contract.service';

describe('FileUploadContractService', () => {
  let service: FileUploadContractService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FileUploadContractService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
