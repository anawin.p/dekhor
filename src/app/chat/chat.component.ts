import { Component, OnInit } from '@angular/core';
import { initializeApp } from 'firebase/app';
import { getAuth, onAuthStateChanged } from 'firebase/auth';
import { getDatabase, ref, onValue, push } from 'firebase/database';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css'],
})
export class ChatComponent implements OnInit {
  firebaseConfig = initializeApp({
    apiKey: 'AIzaSyC0GQGYw6w_YvRQCpVrYJ0zU8dnlA9V9jI',
    authDomain: 'dekhordemo-23dde.firebaseapp.com',
    databaseURL:
      'https://dekhordemo-23dde-default-rtdb.asia-southeast1.firebasedatabase.app',
    projectId: 'dekhordemo-23dde',
    storageBucket: 'dekhordemo-23dde.appspot.com',
    messagingSenderId: '535683564882',
    appId: '1:535683564882:web:e419589efe9241723fe68b',
    measurementId: 'G-GCE1HL0W9B',
  });

  public database = getDatabase(this.firebaseConfig);
  public Datetime = new Date();

  closemsg = false;
  userid = '';
  public Name: string = '';
  public Message = '';
  public MessageList: any = [];
  

  constructor() {
        const current = new Date();
        const timestamp = current.getTime();
    
    const auth = getAuth();

    const MessageRefs = ref(this.database, 'Chat/Msgs/');
    onValue(MessageRefs, (snapshot) => {
      snapshot.forEach((childSnapshot) => {
        // this.MessageList = childSnapshot.key; 
        const childdata = childSnapshot.val();

        this.MessageList.push(childdata);
        const lastmsg = this.MessageList[this.MessageList.length - 1]
        console.log(lastmsg);
        console.log(this.MessageList.length);
      });
    });

    onAuthStateChanged(auth, (user) => {
      if (user) {
        this.userid = user.uid;
        ////////////////Get Name มาใส่//////////
        const Name = ref(this.database, 'Users/' + this.userid);
        onValue(Name, (snapshot) => {
          const Names = snapshot.val();
          this.Name = Names.Name;
        });
      } else if (!user) {
        this.Name = '';
      }
    });
  }

  ngOnInit(): void {}

  texts1() {
    this.MessageList.length=0;
    if (this.Message == null || this.Message == '' || this.Message == ' ') {
      return;
    } else {
      
      push(ref(this.database, 'Chat/Msgs/'), {
        Msg: this.Message,
        Name: this.Name,
      });

      this.Message = '';
      

      setTimeout(() => {
        this.scrooltothelastElementByClassName();
      }, 10);
    }

    // const RoleRefs = ref(this.database, 'Chat/Msgs/' + this.Name);
    // onValue(RoleRefs, (snapshot) => {
    //   snapshot.forEach((childSnapshot) => {
    //     // this.MessageList = childSnapshot.key;
    //     const childdata = childSnapshot.val();
    //     this.MessageList.push(childdata);
    //     console.log(this.MessageList);
    //   });
    // });

    // let MessageList = {
    // Name: this.Name,
    // Msg: this.Message,
    // }
    // this.MessageList.push();

    // this.Message = '';

    // setTimeout(() => {
    //   this.scrooltothelastElementByClassName();
    // }, 10);
  }

  insertname() {
    
    console.log(this.MessageList.length);

    if (!this.Name) {
      let insertName = prompt('Please enter your name:');
      if (insertName == null || insertName == '') {
        alert('กรอกชื่ออีกครั้ง');
        return;
      } else {
        this.closemsg = true;
        this.Name = insertName;
      }
    } else if (this.Name == this.Name) {
      // const RoleRefs = ref(this.database, 'Chat/Msgs/' + this.Name);
      // onValue(RoleRefs, (snapshot) => {
      //   snapshot.forEach((childSnapshot) => {
      //     // this.MessageList = childSnapshot.key;
      //     const childdata = childSnapshot.val();
      //     this.MessageList.push(childdata);
      //     console.log(this.MessageList);
      //   });
      // });

      this.closemsg = true;
    }

    // if (this.Name == '') {
    //   alert('ยังไม่ใส่ชื่อ');
    //   this.closemsg = true;
    // }
    // alert('ยังไม่มีชื่อ');
    // this.closemsg = true;
  }

  scrooltothelastElementByClassName() {
    let element = document.getElementsByClassName('msglength');
    let elementlenght: any = element[element.length - 1];
    let Topset = elementlenght.offsetTop;

    //@ts-ignore
    document.getElementById('context')?.scrollTop = Topset;
  }
}
